#!/bin/bash

PRODUCT_ID=2303
VENDOR_ID=067b
ACTION_CODE=add
SYMLINK_NAME=GPSDevice

TARGET_COMMAND="ACTION=="$ACTION_CODE", ATTRS{idVendor}=="$VENDOR_ID", ATTRS{idProduct}=="$PRODUCT_ID", SYMLINK+="$SYMLINK_NAME""

SCRIPT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
RULE_FILE=99-usb-serial.rules
COPY_PATH=/etc/udev/rules.d/

# First Way

# cd $COPY_PATH
# sudo touch 99-usb-serial.rules
# echo "$TARGET_COMMAND" | sudo tee --append /etc/udev/rules.d/99-usb-serial.rules

# Second Way
cd $SCRIPT_PATH
sudo cp $RULE_FILE $COPY_PATH


# Apply Rules
sudo udevadm control --reload-rules
sudo udevadm trigger