#!/bin/bash
PERCEPTION_DOCKER_IMAGE_NAME=perception_jetson

script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
echo $script_path
cd $script_path
cd ..
cd Jetson
docker build -t $PERCEPTION_DOCKER_IMAGE_NAME .
