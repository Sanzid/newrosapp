#!/bin/bash
cd ~

# Parameters
VOLUME_NAME_HMI=hmi_volume
VOLUME_NAME_PERCEPTION=perception_volume
VOLUME_NAME_GPS=gps_volume

VOLUME_NAME_HMI_PATH=~/hmi_volume
VOLUME_NAME_PERCEPTION_PATH=~/perception_volume
VOLUME_NAME_GPS_PATH=~/gps_volume


# Create Folders
mkdir hmi_volume
mkdir perception_volume
mkdir gps_volume

# Create Volumes
docker volume create --name ${VOLUME_NAME_HMI} --opt type=none --opt device=${VOLUME_NAME_HMI_PATH} --opt o=bind
docker volume create --name ${VOLUME_NAME_PERCEPTION} --opt type=none --opt device=${VOLUME_NAME_PERCEPTION_PATH} --opt o=bind
docker volume create --name ${VOLUME_NAME_GPS} --opt type=none --opt device=${VOLUME_NAME_GPS_PATH} --opt o=bind