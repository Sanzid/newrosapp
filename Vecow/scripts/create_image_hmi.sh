#!/bin/bash
HMI_DOCKER_IMAGE_NAME=hmi_vecow

script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
echo $script_path
cd $script_path
cd ..
cd HMI
docker build -t $HMI_DOCKER_IMAGE_NAME .
