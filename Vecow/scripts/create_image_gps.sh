#!/bin/bash
GPS_DOCKER_IMAGE_NAME=gps_container

script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
echo $script_path
cd $script_path
cd ..
cd GPS
docker build -t $GPS_DOCKER_IMAGE_NAME .