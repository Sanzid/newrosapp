#!/bin/bash
script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd $script_path
# source ./set_port_mirroring_wireless.sh 
source ./create_volumes.sh 
source ./create_image_hmi.sh
source ./create_image_jetson.sh
source ./create_image_gps.sh
