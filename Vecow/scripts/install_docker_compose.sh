#!/bin/bash

## Install Docker-Compose

# Pull the repository
sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

# Install Docker-Compose
sudo chmod +x /usr/local/bin/docker-compose
sudo apt-get install -y docker-ce