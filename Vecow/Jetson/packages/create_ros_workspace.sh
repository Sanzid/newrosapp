#!/bin/bash

# Ros Source 
source /opt/ros/$ROS_DISTRO/setup.sh

if [ ! -d $CHECK_DIR ]; then
    # HMI Clone and Build
    echo "TRYING TO CLONE"
    cd /home/perception_volume
    git clone https://${USERNAME}:${TOKEN}@tmc-droom-gitlab.com/kamigo/tugnova/perception.git
    cd perception
    git checkout $DESIRED_BRANCH
    catkin_make --jobs 1 --pkg autoware_msgs communication_msgs
    if [[ $DESIRED_BRANCH == "classes_multi_check" ]];then
        catkin_make --jobs 1 -DROS_EDITION=ROS1 --pkg livox_ros_driver2
    fi
    catkin_make --jobs 1
else
    if [ $GIT_PULL = true ] ; then
        echo "TRYING TO PULL"
        # Perception Pull, Checkout and Build
        cd /home/perception_volume/perception
        git checkout $DESIRED_BRANCH
        git pull
        catkin_make --jobs 1 --pkg autoware_msgs communication_msgs
        if [[ $DESIRED_BRANCH == "classes_multi_check" ]];then
            catkin_make --jobs 1 -DROS_EDITION=ROS1 --pkg livox_ros_driver2
        fi
        catkin_make --jobs 1
    fi
    if [ $REBUILD = true ] ; then
        echo "Rebuilding files from zero"
        cd /home/perception_volume/perception
        if [ -d $BUILD_DIR ] ; then
            rm -rf build devel
        fi
        git checkout $DESIRED_BRANCH
        git pull
        catkin_make --jobs 1 --pkg autoware_msgs communication_msgs
        if [[ $DESIRED_BRANCH == "classes_multi_check" ]];then
            catkin_make --jobs 1 -DROS_EDITION=ROS1 --pkg livox_ros_driver2
        fi
        catkin_make --jobs 1
    fi
fi

echo "================JETSON CONTAINER COMPILE COMPLETE========================="
    

