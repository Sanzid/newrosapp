#!/bin/bash
set -e

USERNAME=sierra_miguel_Z621473
TOKEN=guHzCvFKJmypqJDpeX1F
DESIRED_BRANCH=$1
GIT_PULL=$2
TUGNOVA_ID=$3
REBUILD=$4
CHECK_DIR=/home/perception_volume/perception
BUILD_DIR=/home/perception_volume/perception/build

ROS_ENV_SOURCE="/opt/ros/$ROS_DISTRO/setup.bash"

echo "sourcing   $ROS_ENV_SOURCE"
source "$ROS_ENV_SOURCE"

echo "ROS_ROOT   $ROS_ROOT"
echo "ROS_DISTRO $ROS_DISTRO"

source ./create_ros_workspace.sh 

export PATH=/usr/local/cuda-10.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:~/.local/bin
export LD_LIBRARY_PATH=/usr/local/cuda-10.0/lib64:

export ROS_MASTER_URI=http://192.168.0.37:11311
export ROS_IP=192.168.0.37

BASE_DIR=/home/perception_volume
WORKSPACE_ENV_SOURCE="$BASE_DIR/perception/devel/setup.bash"

echo "sourcing   $WORKSPACE_ENV_SOURCE"
source "$WORKSPACE_ENV_SOURCE"

echo "ROS_ROOT   $ROS_ROOT"
echo "ROS_DISTRO $ROS_DISTRO"

echo ${WORKSPACE_ENV_SOURCE} >> ~/.bashrc

echo "$(date): perception ros docker started" 

cd /

sleep 2
roslaunch livox_ros_driver lidar_fusion_${TUGNOVA_ID}gouki.launch --wait
